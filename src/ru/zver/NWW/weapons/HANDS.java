package ru.zver.NWW.weapons;

/**
 * Created by Папа on 19.09.2017.
 */
public enum HANDS {
    ONE_HAND{
        @Override
        public String toString() {
            return "Одноручное";
        }
    },
    TWO_HAND{
        @Override
        public String toString() {
            return "Двуручное";
        }
    }
}
