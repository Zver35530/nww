package ru.zver.NWW.weapons;

import ru.zver.NWW.ADDON;

/**
 * Created by VZverev on 25.08.2017.
 */
public class Item {
    final public double weigth, cost;
    final public String name;
    final public ADDON addon;

    protected Item(double weigth, double cost, String name, ADDON addon) {
        this.weigth = weigth;
        this.cost = cost;
        this.name = name;
        this.addon = addon;
    }

    protected Item(double weigth, double cost, String name) {
        this.weigth = weigth;
        this.cost = cost;
        this.name = name;
        this.addon = ADDON.NEW_VEGAS;
    }

    @Override
    public String toString() {
        return name + "\t" + weigth + "\t" + cost;
    }
}
