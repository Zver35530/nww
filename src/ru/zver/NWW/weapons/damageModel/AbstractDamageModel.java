package ru.zver.NWW.weapons.damageModel;

import ru.zver.NWW.PERK;

/**
 * Created by VZverev on 05.09.2017.
 */
public class AbstractDamageModel {
    public final double damage, DPS;
    private final PERK[] perks;

    public AbstractDamageModel(double damage, double DPS, PERK ... perks) {
        this.damage = damage;
        this.DPS = DPS;
        this.perks = perks;
    }

    public double getDamage() {
        return damage;
    }

    public double getDPS() {
        return DPS;
    }

    public int getAmmoInShot(){
        return 1;
    }

    public PerkDamage[] getDamageForPerks(){
        if(perks.length == 0)
            return new PerkDamage[0];

        PerkDamage[] perkDamages = new PerkDamage[perks.length];

        int iPerk = 0;
        for(PERK perk: perks){
            perkDamages[iPerk++] = new PerkDamage(perk);
        }

        return perkDamages;
    }

    public class PerkDamage {
        public final PERK perk;
        public final double damage, DPS;

        public PerkDamage(PERK perk) {
            this.perk = perk;
            this.damage = AbstractDamageModel.this.damage * perk.getDamModif() ;
            this.DPS =  AbstractDamageModel.this.DPS * perk.getDamModif();
        }
    }
}
