package ru.zver.NWW.weapons.damageModel;

import ru.zver.NWW.PERK;
import ru.zver.NWW.weapons.damageModel.AbstractDamageModel;

/**
 * Created by VZverev on 05.09.2017.
 */
public class ShotgunGamageModel extends AbstractDamageModel {
    public final int ammoInShot;

    public ShotgunGamageModel(double damage, double DPS, int ammoInShot, PERK ... perks) {
        super(damage, DPS, perks);
        this.ammoInShot = ammoInShot;
    }

    @Override
    public int getAmmoInShot() {
        return ammoInShot;
    }

    @Override
    public double getDamage() {
        return super.getDamage();
    }
}
