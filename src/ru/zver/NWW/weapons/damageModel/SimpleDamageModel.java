package ru.zver.NWW.weapons.damageModel;

import ru.zver.NWW.PERK;
import ru.zver.NWW.weapons.damageModel.AbstractDamageModel;

/**
 * Created by VZverev on 05.09.2017.
 */
public class SimpleDamageModel extends AbstractDamageModel {
    public SimpleDamageModel(double damage, double DPS, PERK ... perks) {
        super(damage, DPS, perks);
    }
}
