package ru.zver.NWW.weapons.damageModel;

import ru.zver.NWW.PERK;

/**
 * Created by VZverev on 07.09.2017.
 */
public class GunDamageModel extends AbstractDamageModel {
    public GunDamageModel(double damage, double DPS, PERK... perks) {
        super(damage, DPS, perks);
    }
}
