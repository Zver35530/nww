package ru.zver.NWW.weapons.damageModel;

import ru.zver.NWW.PERK;
import ru.zver.NWW.weapons.AbstractWeapon;

/**
 * Created by VZverev on 05.09.2017.
 */
public class EnergyDamageModel extends AbstractDamageModel {

    final public int ammoPerShot;

    public EnergyDamageModel(double damage, double DPS, int ammoPerShot, PERK ... perks) {
        super(damage, DPS, perks);
        this.ammoPerShot = ammoPerShot;
    }
}
