package ru.zver.NWW.weapons;

import ru.zver.NWW.ammo.GunAmmo;
import ru.zver.NWW.weapons.AbstractGunAmmoWeapon;
import ru.zver.NWW.weapons.Perks;
import ru.zver.NWW.weapons.damageModel.GunDamageModel;

/**
 * Created by VZverev on 25.08.2017.
 */
public class GunWeapon extends AbstractGunAmmoWeapon {

    public GunWeapon(Weapon weapon, GunDamageModel gunDamageModel, int durance, int AP, HANDS hands, GunAmmo ammo, int capacity, double spread) {
        super(weapon, gunDamageModel, durance, AP, hands, ammo, capacity, spread);
    }

}
