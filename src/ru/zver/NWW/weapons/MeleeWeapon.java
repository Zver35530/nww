package ru.zver.NWW.weapons;

import ru.zver.NWW.PERK;
import ru.zver.NWW.weapons.damageModel.AbstractDamageModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VZverev on 25.08.2017.
 */
public class MeleeWeapon extends AbstractWeapon {

    @Override
    public String getCategoryName() {
        return "Холодное оружие";
    }

    public MeleeWeapon(Weapon weapon, AbstractDamageModel damageModel, int durance, int AP, HANDS hands) {
        super(weapon, damageModel, durance, AP, hands);
    }

    @Override
    public double getWeigthForDamage(double damage) {
        return weapon.weigth;
    }
}
