package ru.zver.NWW.weapons;

import ru.zver.NWW.ADDON;
import ru.zver.NWW.weapons.damageModel.EnergyDamageModel;

/**
 * Created by VZverev on 01.09.2017.
 */
public class PlasmaWeapon extends AbstractEnergyWeapon {
    public PlasmaWeapon(Weapon weapon, EnergyDamageModel energyDamageModel, int durance, int AP, HANDS hands, EnergyAmmo energyAmmo, int ammoCap, double spread, int ammoPerShot) {
        super(weapon, energyDamageModel, durance, AP, hands, energyAmmo, ammoCap, spread, ammoPerShot);
    }
}
