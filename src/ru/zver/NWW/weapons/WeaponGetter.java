package ru.zver.NWW.weapons;

import ru.zver.NWW.ADDON;
import ru.zver.NWW.PERK;
import ru.zver.NWW.ammo.ShotgunAmmo;
import ru.zver.NWW.weapons.damageModel.ShotgunGamageModel;
import ru.zver.NWW.weapons.damageModel.SimpleDamageModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VZverev on 04.09.2017.
 */
public class WeaponGetter {

    public List<ShotgunWeapon> getShotguns(){
        List<ShotgunWeapon> shotgunWeaponList = new ArrayList<>();

      //  shotgunWeaponList.add(new ShotgunWeapon(10, 400, "Дробовик с рычажной перезарядкой", new ShotgunGamageModel(5, 60, 8, PERK.COWBOY), 80, ShotgunAmmo.getShotgun12(), 7, 0.2 ));

        return shotgunWeaponList;
    }

    public List<MeleeWeapon> getMelee(){
        List<MeleeWeapon> meleeWeapons = new ArrayList<>();

        meleeWeapons.add( new MeleeWeapon( new Weapon( 1, 900, "Нож шанса"), new SimpleDamageModel(22, 91, PERK.COWBOY), 90, 17, HANDS.ONE_HAND));
        meleeWeapons.add( new MeleeWeapon( new Weapon( 12, 2_500, "Большой меч"), new SimpleDamageModel(32, 45.5), 300, 38, HANDS.TWO_HAND));
        meleeWeapons.add( new MeleeWeapon( new Weapon( 12, 45, "Клинок Востока"), new SimpleDamageModel(65, 97.5), 800, 35, HANDS.TWO_HAND));
        meleeWeapons.add( new MeleeWeapon( new Weapon( 12, 2_800, "Цепная пила (GRA)", ADDON.GUN_RUNNERS_ARSENALE), new SimpleDamageModel(95, 95), 2400, 65, HANDS.TWO_HAND));
        meleeWeapons.add( new MeleeWeapon( new Weapon( 8, 3_200, "Тук-тук"), new SimpleDamageModel(66, 125), 110, 21, HANDS.TWO_HAND));

        return meleeWeapons;
    }
}
