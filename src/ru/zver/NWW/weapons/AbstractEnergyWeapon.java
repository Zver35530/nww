package ru.zver.NWW.weapons;

import ru.zver.NWW.ADDON;
import ru.zver.NWW.ammo.AbstractAmmo;
import ru.zver.NWW.weapons.damageModel.EnergyDamageModel;
import ru.zver.NWW.weapons.damageModel.SimpleDamageModel;

/**
 * Created by VZverev on 01.09.2017.
 */
public abstract class AbstractEnergyWeapon extends AbstractGunAmmoWeapon {

    public final int ammoPerShot;

    public AbstractEnergyWeapon(Weapon weapon, EnergyDamageModel damageModel, int durance, int AP, HANDS hands, AbstractAmmo ammo, int ammoCapacity, double spread, int ammoPerShot) {
        super(weapon, damageModel, durance, AP, hands, ammo, ammoCapacity, spread);
        this.ammoPerShot = ammoPerShot;
    }

    @Override
    public double getWeigthForDamage(double damage) {
        return super.getWeigthForDamage(damage) * ammoPerShot;
    }
}

class EnergyAmmo extends AbstractAmmo {
    public EnergyAmmo(double weigth, double cost, String name, ADDON addon) {
        super(weigth, cost, name, addon);
    }
}