package ru.zver.NWW.weapons;

import ru.zver.NWW.ADDON;
import ru.zver.NWW.ammo.AbstractAmmo;
import ru.zver.NWW.weapons.AbstractWeapon;
import ru.zver.NWW.weapons.Perks;
import ru.zver.NWW.weapons.damageModel.AbstractDamageModel;
import ru.zver.NWW.weapons.damageModel.SimpleDamageModel;

import java.text.SimpleDateFormat;

/**
 * Created by VZverev on 25.08.2017.
 */
public abstract class AbstractGunAmmoWeapon extends AbstractWeapon {
    public final AbstractAmmo ammo;
    public final int ammoCapacity;
    public final double spread;

    public AbstractGunAmmoWeapon(Weapon weapon, AbstractDamageModel damageModel, int durance, int AP, HANDS hands, AbstractAmmo ammo, int ammoCapacity, double spread) {
        super(weapon, damageModel, durance, AP, HANDS.ONE_HAND);
        this.ammo = ammo;
        this.ammoCapacity = ammoCapacity;
        this.spread = spread;
    }


    @Override
    public double getWeigthForDamage(double damage) {
        return weapon.weigth + damage / getDamage() * ammo.weigth;
    }

    @Override
    public String getCategoryName() {
        return "Огнестрельное оружие";
    }
}
