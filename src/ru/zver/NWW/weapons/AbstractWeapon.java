package ru.zver.NWW.weapons;

import ru.zver.NWW.PERK;
import ru.zver.NWW.weapons.damageModel.AbstractDamageModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VZverev on 25.08.2017.
 */
public abstract class AbstractWeapon  {
    public final AbstractDamageModel damageModel;
    final int durance, AP;
    public final HANDS hands;
    public final Weapon weapon;

    public AbstractWeapon(Weapon weapon, AbstractDamageModel damageModel, int durance, int AP, HANDS hands) {
        this.weapon = weapon;
        this.durance = durance;
        this.damageModel = damageModel;
        this.AP = AP;
        this.hands = hands;
    }

    public double getDamage(){
        return damageModel.damage;
    }

    public double getDPS(){
        return damageModel.DPS;
    }

    public double totalDamageBeforeBroken(){
        return damageModel.damage * durance / 0.02;
    }

    public double totalDamageBeforeBroken(AbstractDamageModel.PerkDamage perkDamage){
        return totalDamageBeforeBroken() * perkDamage.perk.getDamModif();
    }

    public abstract double getWeigthForDamage(double damage);

    public double getWeigthForDamage(double damage, AbstractDamageModel.PerkDamage perkDamage){
        return getWeigthForDamage(damage) / perkDamage.perk.getDamModif();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.
                append(super.toString()).append("\t").
                append(getCategoryName()).append("\t").
                append(getDamage()).append("\t").
                append(damageModel.DPS).append("\t").
                append(getWeigthForDamage(1_000)).append("\t").
                append(totalDamageBeforeBroken());

        for(AbstractDamageModel.PerkDamage perkDamage: damageModel.getDamageForPerks()){
            builder.append("\n").
                    append(super.toString()).append("\t").
                    append(getCategoryName()).append("\t").
                    append(perkDamage.damage).append("\t").
                    append(perkDamage.DPS).append("\t").
                    append(getWeigthForDamage(1_000, perkDamage)).append("\t").
                    append(totalDamageBeforeBroken(perkDamage)).append("\t").
                    append(perkDamage.perk.getName());
        }
        return builder.toString();
    }

    public abstract String getCategoryName();
}

class Perks{
    public final PERK[] perks;

    public Perks(PERK ... perks){
        this.perks = perks;
    }

    public List<? extends AbstractWeapon> getPerWeapons(){
        List<? extends AbstractWeapon> weapons = new ArrayList<>();

        for(PERK perk: perks){

        }

        return weapons;
    }
}


