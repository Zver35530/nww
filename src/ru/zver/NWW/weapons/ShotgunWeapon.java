package ru.zver.NWW.weapons;

import ru.zver.NWW.ammo.AbstractAmmo;
import ru.zver.NWW.weapons.damageModel.ShotgunGamageModel;

/**
 * Created by VZverev on 01.09.2017.
 */
public class ShotgunWeapon extends AbstractGunAmmoWeapon {

    public ShotgunWeapon(Weapon weapon, ShotgunGamageModel damageModel, int durance, int AP, HANDS hands, AbstractAmmo ammo, int ammoCapacity, double spread) {
        super(weapon, damageModel, durance, AP, hands, ammo, ammoCapacity, spread);
    }

    @Override
    public double getWeigthForDamage(double damage) {
        return super.getWeigthForDamage(damage)/damageModel.getAmmoInShot();
    }
}
