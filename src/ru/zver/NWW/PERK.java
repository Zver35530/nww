package ru.zver.NWW;

/**
 * Created by VZverev on 04.09.2017.
 */
public enum PERK{

        COWBOY{
            @Override
            public String getName() {
                return "Ковбой";
            }

            @Override
            public double getDamModif(){return 1.25;}
    },
    PYRO{
        @Override
        public double getDamModif() {
            return 1.5;
        }

        @Override
        public String getName() {
            return "Пироман";
        }
    },
    LASER{
        @Override
        public String getName() {
            return "Лазеров начальник";
        }

        @Override
        public double getDamModif() {
            return 1.15;
        }
    }

    ;
    public abstract double getDamModif();
    public abstract String getName();
}
