package ru.zver.NWW.ammo;

import ru.zver.NWW.ADDON;

/**
 * Created by VZverev on 04.09.2017.
 */
public class ShotgunAmmo extends AbstractAmmo {

    public static ShotgunAmmo getShotgun12(){
        return new ShotgunAmmo(0.2, 2, "Дробь, 12", ADDON.NEW_VEGAS);
    }

    private ShotgunAmmo(double weigth, double cost, String name, ADDON addon) {
        super(weigth, cost, name, addon);
    }
}