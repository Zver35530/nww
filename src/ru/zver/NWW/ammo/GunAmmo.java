package ru.zver.NWW.ammo;

import ru.zver.NWW.ADDON;

/**
 * Created by VZverev on 04.09.2017.
 */
public class GunAmmo extends AbstractAmmo {

    public GunAmmo get357(){
        return new GunAmmo(0.02, 1, "0.357" );
    }

    private GunAmmo(double weigth, double cost, String name,ADDON addon) {
        super(weigth, cost, name, addon);
    }

    private GunAmmo(double weigth, double cost, String name) {
        super(weigth, cost, name, ADDON.NEW_VEGAS);
    }
}
