package ru.zver.NWW.weaponLoaders;

import ru.zver.NWW.weapons.AbstractWeapon;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractWeaponLoader {

    private final String fileName;
    private final File file;
    private final String[] headerRow;

    private final int namePos;
    private final int weithPos;
    private final int privePos;
    private final int damagePos;
    private final int dpsPos;
   // private final int apsPos;
    private final int critChancePos;
    private final int apPos;
    private final int durancePos;
    private final int paramValPos;
    private final int skillPos;

    private final BufferedReader bufferedReader;

    public AbstractWeaponLoader(String fileName) throws IOException {
        this.fileName = fileName;
        file = new File("res/" + fileName);

        bufferedReader = new BufferedReader(new FileReader(file));
        headerRow = bufferedReader.readLine().split(" ");

        if(headerRow.length == 0)
            throw new IOException("Строка с заголовками не найдена");

        namePos         = getColumnNmbByName("НАЗВАНИЕ");
        weithPos        = getColumnNmbByName("ВЕС");
        privePos        = getColumnNmbByName("СТО");
        damagePos       = getColumnNmbByName("УРО/ВЫСТР");
        dpsPos          = getColumnNmbByName("УВС");
        //apsPos          = getColumnNmbByName("АВС");
        critChancePos   = getColumnNmbByName("МНОЖКРИТ");
        apPos           = getColumnNmbByName("ОД");
        durancePos      = getColumnNmbByName("HP");
        paramValPos     = getColumnNmbByName("Треб");
        skillPos        = getColumnNmbByName("Навык");
    }

    private int getColumnNmbByName(String name) throws IOException {
        for(int iColumn = 0; iColumn < headerRow.length; iColumn++){
            if(headerRow[iColumn].toLowerCase().equals(name.toLowerCase()))
                return iColumn;
        }

        throw new IOException("Заголовок с названием " + name + " не найден");
    }

    public List<AbstractWeapon> getWeaponsFromFile(){
        String curString;
        String[] splitetdString;
        try {
            bufferedReader.readLine(); //строка заголовка

            while( (curString = bufferedReader.readLine()) != null){
                splitetdString = curString.split(" ");

            }

            return new ArrayList<AbstractWeapon>();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ArrayList<AbstractWeapon>();
    }


}
