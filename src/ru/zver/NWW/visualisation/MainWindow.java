package ru.zver.NWW.visualisation;

import javax.swing.*;

import javafx.geometry.HorizontalDirection;
import ru.zver.NWW.*;
import ru.zver.NWW.weapons.AbstractWeapon;
import ru.zver.NWW.weapons.HANDS;
import ru.zver.NWW.weapons.Weapon;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by VZverev on 04.09.2017.
 */
public class MainWindow extends JFrame {

    public MainWindow(java.util.List<AbstractWeapon> weapons){

        super("График");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setLayout(new BorderLayout());

        setVisible(true);

        JPanel filters = new JPanel();
        filters.setLayout(new BoxLayout(filters, BoxLayout.X_AXIS));

        filters.add(new AbstractFilterJFrame(PERK.values()) {
            @Override
            public String getStringValueForWeapon(AbstractWeapon weapon) {
                return "";
            }
        });

        filters.add(new AbstractFilterJFrame(HANDS.values()) {
            @Override
            public String getStringValueForWeapon(AbstractWeapon weapon) {
                return weapon.hands.toString();
            }
        });

        Set<String> weaponsTypes = new HashSet<>();
        for(AbstractWeapon weapon: weapons){
            weaponsTypes.add(weapon.getCategoryName());
        }
        filters.add(new AbstractFilterJFrame(weaponsTypes.toArray()) {
            @Override
            public String getStringValueForWeapon(AbstractWeapon weapon) {
                return weapon.getCategoryName();
            }
        });

        this.add(filters, BorderLayout.PAGE_START);

        GraphPanel graphPanel = new GraphPanel(
                weapons,
                new WeaponParamGetter() {
                    @Override
                    public double getValue(AbstractWeapon weapon) {
                        return weapon.getDamage();
                    }
                }, new WeaponParamGetter() {
            @Override
            public double getValue(AbstractWeapon weapon) {
                return weapon.getDPS();
            }
        });

        this.add(graphPanel, BorderLayout.CENTER);

        this.add(new WeaponValueJPanel(graphPanel), BorderLayout.LINE_START);
        this.add(new WeaponValueJPanel(graphPanel), BorderLayout.LINE_END);

        this.pack();
    }



    public class WeaponValueJPanel extends JPanel{
        ButtonGroup valueGroup = new ButtonGroup();

        private final GraphPanel graphPanel;

        {
            this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

            WeaponValueSelector weaponValueSelector = new WeaponValueSelector(
                    "Урон",
                    new WeaponParamGetter() {
                        @Override
                        public double getValue(AbstractWeapon weapon) {
                            return weapon.getDamage();
                        }
                    });

            this.add(weaponValueSelector);
            valueGroup.add(weaponValueSelector);

            weaponValueSelector = new WeaponValueSelector(
                    "УВС",
                    new WeaponParamGetter() {
                        @Override
                        public double getValue(AbstractWeapon weapon) {
                            return weapon.getDPS();
                        }
                    });

            this.add(weaponValueSelector);
            valueGroup.add(weaponValueSelector);
        }

        public WeaponValueJPanel(GraphPanel graphPanel){
            this.graphPanel = graphPanel;
        }


    }
}
