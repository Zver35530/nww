package ru.zver.NWW.visualisation;

import javax.swing.*;

public class WeaponValueSelector extends JRadioButton{
    private final WeaponParamGetter weaponParamGetter;
    public final String name;

    public WeaponValueSelector(String name, WeaponParamGetter weaponParamGetter){
        this.weaponParamGetter = weaponParamGetter;
        this.name = name;

        this.setText(name);
    }
}
