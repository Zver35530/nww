package ru.zver.NWW.visualisation;

import ru.zver.NWW.weapons.AbstractWeapon;

public abstract class WeaponParamGetter {
    public abstract double getValue(AbstractWeapon weapon);
}
