package ru.zver.NWW.visualisation;

import ru.zver.NWW.weapons.AbstractWeapon;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by VZverev on 07.09.2017.
 */
public class GraphPanel extends JPanel {

    private java.util.List<Coordinate> coordinateList = new ArrayList<>();
    private double[] x_values, y_values;
    private WeaponParamGetter weaponParamGetter_x, weaponParamGetter_y;
    private final java.util.List<AbstractWeapon> weaponList;

    Comparator<Coordinate> coordinateComparator = new Comparator<Coordinate>() {
        @Override
        public int compare(Coordinate o1, Coordinate o2) {
            return o1.x < o2.x ? -1 : 1;
        }
    };

    public GraphPanel(java.util.List<AbstractWeapon> weaponList, WeaponParamGetter weaponParamGetter_x, WeaponParamGetter weaponParamGetter_y){
        setPreferredSize(new Dimension(200, 200));
        this.weaponParamGetter_x = weaponParamGetter_x;
        this.weaponParamGetter_y = weaponParamGetter_y;

        this.weaponList = weaponList;
        x_values = new double[weaponList.size()];
        y_values = new double[weaponList.size()];

        for(AbstractWeapon weapon: weaponList){
            coordinateList.add( new Coordinate(weaponParamGetter_x.getValue(weapon), weaponParamGetter_y.getValue(weapon)));
        }
    }

    public void setWeaponParamGetter_x(WeaponParamGetter weaponParamGetter_x) {
        this.weaponParamGetter_x = weaponParamGetter_x;
    }

    public void setWeaponParamGetter_y(WeaponParamGetter weaponParamGetter_y) {
        this.weaponParamGetter_y = weaponParamGetter_y;
    }

    private void drawGraph(Graphics2D g2){
        if(coordinateList.size() == 0)
            return;

        coordinateList.sort(coordinateComparator);

        double maxX = coordinateList.get(coordinateList.size() - 1).x, maxY = coordinateList.get(0).y;
        for(Coordinate coordinate: coordinateList){
            if(coordinate.y > maxY)
                maxY = coordinate.y;
        }

        for(Coordinate coordinate: coordinateList){
            System.out.println( coordinate.x / maxX * this.getWidth());
            g2.drawOval((int) (coordinate.x / maxX * this.getWidth()),this.getHeight() - (int) (coordinate.y * maxY / getWidth()), 5, 5);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;

        drawGraph(g2);

    }

    class Coordinate{
        public final double x, y;

        public Coordinate(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }
}
