package ru.zver.NWW.visualisation;

import ru.zver.NWW.weapons.AbstractWeapon;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Папа on 21.09.2017.
 */
public abstract class AbstractFilterJFrame extends JPanel {
    private List<String> values = new ArrayList<String>();


    AbstractFilterJFrame(Object[] values){
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        for(Object value: values){
            this.values.add(value.toString());

            this.add(new JCheckBox(value.toString()));
        }

    };

    public abstract String getStringValueForWeapon(AbstractWeapon weapon);

}
