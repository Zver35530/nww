package ru.zver.NWW;

import ru.zver.NWW.visualisation.MainWindow;
import ru.zver.NWW.weaponLoaders.MeleeWeaponLoader;
import ru.zver.NWW.weapons.AbstractWeapon;
import ru.zver.NWW.weapons.WeaponGetter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by VZverev on 27.08.2017.
 */
public class Process {

    public static void main(String[] args){

        List<AbstractWeapon> abstractWeaponList = new ArrayList<>();

        WeaponGetter weaponGetter = new WeaponGetter();

        abstractWeaponList.addAll( weaponGetter.getMelee());
        abstractWeaponList.addAll( weaponGetter.getShotguns());

        for(AbstractWeapon weapon: abstractWeaponList){
            System.out.println(weapon.toString());
        }

        try {
            MeleeWeaponLoader meleeWeaponLoader = new MeleeWeaponLoader("meleeWeapons.csv");

            meleeWeaponLoader.getWeaponsFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        new MainWindow(abstractWeaponList);

    }
}
